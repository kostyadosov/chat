﻿namespace Chat.Models {
    export class Me {
        get Username(): string {
            return localStorage["Chat-Username"]; 
        }
        set Username(Name: string) {
            localStorage["Chat-Username"] = Name;
        }
        get DisplayName(): string {
            return (document.getElementById("inputMyDisplayName") as HTMLInputElement).value;
        }
        set DisplayName(Name: string) {
            (document.getElementById("inputMyDisplayName") as HTMLInputElement).value = Name;
        }
        get AuthenticationToken(): string {
            return localStorage["Chat-AuthenticationToken"];
        }
        set AuthenticationToken(Token:string) {
            localStorage["Chat-AuthenticationToken"] = Token;
        }
        ClearCreds() {
            Chat.Me.AuthenticationToken = null;
            Chat.Me.DisplayName = null;
            Chat.Me.Username = null;
        }
    }
}
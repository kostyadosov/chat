var Chat;
(function (Chat) {
    var Models;
    (function (Models) {
        class Me {
            get Username() {
                return localStorage["Chat-Username"];
            }
            set Username(Name) {
                localStorage["Chat-Username"] = Name;
            }
            get DisplayName() {
                return document.getElementById("inputMyDisplayName").value;
            }
            set DisplayName(Name) {
                document.getElementById("inputMyDisplayName").value = Name;
            }
            get AuthenticationToken() {
                return localStorage["Chat-AuthenticationToken"];
            }
            set AuthenticationToken(Token) {
                localStorage["Chat-AuthenticationToken"] = Token;
            }
            ClearCreds() {
                Chat.Me.AuthenticationToken = null;
                Chat.Me.DisplayName = null;
                Chat.Me.Username = null;
            }
        }
        Models.Me = Me;
    })(Models = Chat.Models || (Chat.Models = {}));
})(Chat || (Chat = {}));
//# sourceMappingURL=me.js.map
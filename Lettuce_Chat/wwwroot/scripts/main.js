var Chat;
(function (Chat) {
    Chat.LogoutExpected = false;
    Chat.Me = new Chat.Models.Me();
    Chat.IsFocused = true;
    Chat.UnreadMessageCount = 0;
    Chat.UnreadMessageInterval = -1;
    function Init() {
        if (Chat.Socket != undefined && Chat.Socket.readyState != WebSocket.CLOSED) {
            throw "WebSocket already exists and is not closed.";
        }
        Chat.Socket = new WebSocket(location.origin.replace("http", "ws") + "/Socket");
        window.onblur = function () {
            Chat.IsFocused = false;
        };
        window.onfocus = function () {
            Chat.IsFocused = true;
            Chat.UnreadMessageCount = 0;
            document.title = "VironIT Chat";
        };
        SetHandlers();
    }
    Chat.Init = Init;
    function SetHandlers() {
        Chat.Socket.onopen = function (e) {
            var queryString = location.search.replace("?", "");
            var query = queryString.split("=");
            if (query.length == 2 && query[0] == "chat") {
                Chat.Messages.JoinChat(query[1]);
            }
            else {
                if (Chat.Me.Username && Chat.Me.AuthenticationToken) {
                    Chat.Messages.TryResumeLogin();
                }
            }
        };
        Chat.Socket.onclose = function (e) {
            if (!Chat.LogoutExpected) {
                Chat.Utilities.ShowDialog("Connection Closed", "Your connection has been closed.  Click OK to reconnect.", function () { location.reload(); });
            }
            Chat.LogoutExpected = false;
        };
        Chat.Socket.onerror = function (e) {
            if (!Chat.LogoutExpected) {
                Chat.Utilities.ShowDialog("Connection Closed", "Your connection has been closed.  Click OK to reconnect.", function () { location.reload(); });
            }
            Chat.LogoutExpected = false;
        };
        Chat.Socket.onmessage = function (e) {
            var message = JSON.parse(e.data);
            Chat.Messages.HandleMessage(message);
        };
    }
    Chat.SetHandlers = SetHandlers;
})(Chat || (Chat = {}));
window.onload = function () {
    Chat.Init();
};
//# sourceMappingURL=main.js.map
﻿namespace Chat {
    export var LogoutExpected = false;
    export var Socket: WebSocket;
    export var Me = new Chat.Models.Me();
    export var IsFocused = true;
    export var UnreadMessageCount = 0;
    export var UnreadMessageInterval = -1;
    export function Init() {
        if (Socket != undefined && Socket.readyState != WebSocket.CLOSED) {
            throw "WebSocket already exists and is not closed.";
        }
        Socket = new WebSocket(location.origin.replace("http", "ws") + "/Socket");
        window.onblur = function () {
            Chat.IsFocused = false;
        }
        window.onfocus = function () {
            Chat.IsFocused = true;
            Chat.UnreadMessageCount = 0;
            document.title = "VironIT Chat";
        }
        SetHandlers();
    }
    export function SetHandlers() {
        Socket.onopen = function (e) {
            var queryString = location.search.replace("?", "");
            var query = queryString.split("=");
            if (query.length == 2 && query[0] == "chat") {
                Chat.Messages.JoinChat(query[1]);
            }
            else {
                if (Chat.Me.Username && Chat.Me.AuthenticationToken) {
                    Chat.Messages.TryResumeLogin();
                }
            }
        };
        Socket.onclose = function (e) {
            if (!LogoutExpected) {
                Chat.Utilities.ShowDialog("Connection Closed", "Your connection has been closed.  Click OK to reconnect.", function () { location.reload() });
            }
            LogoutExpected = false;
        };
        Socket.onerror = function (e) {
            if (!LogoutExpected) {
                Chat.Utilities.ShowDialog("Connection Closed", "Your connection has been closed.  Click OK to reconnect.", function () { location.reload() });
            }
            LogoutExpected = false;
        };
        Socket.onmessage = function (e) {
            var message = JSON.parse(e.data);
            Chat.Messages.HandleMessage(message);
        };
    }
}

window.onload = function () {
    Chat.Init();
}